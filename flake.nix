{
  description = "Tad Lispy's Clojure Playground";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-compat, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        fetchElmDeps = pkgs.elmPackages.fetchElmDeps {
          elmPackages = import ./elm-packages.nix;
          registryDat = ./elm-registry.dat;
          elmVersion = (builtins.fromJSON (
            builtins.readFile ./elm.json
          )).elm-version;
        };
        shared-inputs = [
            pkgs.gnumake
            pkgs.clojure
        ];
      in rec {
        packages.clojure-playground = pkgs.stdenv.mkDerivation {
          name = "clojure-playground";
          src = self;
          buildInputs = shared-inputs ++ [];
        };

        devShell = pkgs.mkShell {
          name = "clojure-playground-dev-shell";
          src = self;
          buildInputs = shared-inputs ++ [
            pkgs.cachix
          ];
        };

        defaultPackage = packages.clojure-playground;
      }
    );
}
