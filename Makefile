include Makefile.d/defaults.mk

all: ## Run unit tests and build the program (DEFAULT)
all: dist
.PHONY: all

help: ## Print this help message
help: # TODO: Handle section headers in awk script
	@echo "Useage: make [ goals ]"
	@echo
	@echo "Available goals:"
	@echo
	@cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

dist: ## Build the program
	$(error "Not implemented")
.PHONY: dist

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install

### DEVELOPMENT

repl: ## Start Clojure repl
repl:
	clojure -M:rebel
.PHONY: repl

develop: ## Start the program in development mode
develop:
	$(error "Not implemented")
.PHONY: develop

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)
